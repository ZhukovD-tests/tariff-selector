const getTariffs = async () => {
    let req = new XMLHttpRequest();
    req.open('GET', 'http://test/tarifs/api/tariffs');
    req.send();

    let result = await new Promise((resolve, reject) => {
        req.onload = () => {
            if (req.status != 200) {
                console.log(`Ошибка ${req.status}: ${req.statusText}`);
                reject();
            } else {
                resolve(JSON.parse(req.response).tarifs);
            }
        }
    })

    return result;
}

const buildVariantDOM = (data, isFull = false, groupIndex) => {
    let DOMCont = document.createElement("div"),
        DOMTitle = document.createElement("h3"),
        DOMPayPeriod,
        DOMPriceMontly = document.createElement("span"),
        DOMInfoBlock = document.createElement("div"),
        DOMInfoPrice = document.createElement("li"),
        DOMInfoDiscount = document.createElement("li"),
        DOMInfoList = document.createElement("ul"),
        DOMInfoPriceAdd,
        DOMMoreInfoBlock,
        DOMMoreWillStart,
        DOMMoreActiveBefore,
        DOMBtnWrapper,
        DOMBtn = document.createElement("button");

    if (isFull) {
        DOMPayPeriod = document.createElement("span");
        DOMMoreInfoBlock = document.createElement("ul");
        DOMMoreInfoBlock.className = "info__list low_opacity";
        DOMMoreWillStart = document.createElement("li");
        DOMMoreActiveBefore = document.createElement("li");

        DOMMoreWillStart.innerText = "вступит в силу - сегодня";
        DOMMoreActiveBefore.innerText = `активно до - ${data.new_payday}`;
        DOMMoreInfoBlock.appendChild(DOMMoreWillStart);
        DOMMoreInfoBlock.appendChild(DOMMoreActiveBefore);
    }

    DOMCont.className = "variants__elem variant";

    DOMTitle.className = "variant__title";
    let months = "месяц";
    if (data.pay_period == 1)
        months = "месяц";
    else if (data.pay_period < 4)
        months = "месяца";
    else
        months = "месяцев";

    if (isFull) {
        DOMTitle.innerText = `Тариф "${data.title.replace(/ \(.+\)$/i, "")}"`;
        DOMPayPeriod.innerText = `Период оплаты - ${data.pay_period} ${months}`;
        DOMCont.appendChild(DOMTitle);
        DOMInfoBlock.appendChild(DOMPayPeriod);
    } else {
        DOMTitle.innerText = `${data.pay_period} ${months}`;
        DOMCont.appendChild(DOMTitle);
    }

    DOMInfoBlock.className = "variant__info info";
    DOMPriceMontly.className = "info__priceMonthly";
    DOMPriceMontly.innerText = `${data.priceMonthly} ₽/мес`;
    DOMInfoBlock.appendChild(DOMPriceMontly);
    DOMInfoPrice.innerText = `разовый платеж - ${data.price} ₽`;
    DOMInfoList.appendChild(DOMInfoPrice);
    if (isFull) {
        DOMInfoPriceAdd = document.createElement("li");
        DOMInfoPriceAdd.innerText = `со счета спишется - ${data.price} ₽`;
        DOMInfoList.appendChild(DOMInfoPriceAdd);
        DOMBtn.className = "variant-full__select-btn";
        DOMBtn.onclick = (e) => {
            DOMBtn.innerHTML = "Выбрано!";
            DOMBtn.disabled = true;
        };
        DOMBtn.innerText = "Выбрать";
        DOMBtnWrapper = document.createElement("div");
        DOMBtnWrapper.className = "variant-full__select-btn-wrapper";
        DOMBtnWrapper.appendChild(DOMBtn);
    } else {
        if (data.totalDiscount > 0) {
            DOMInfoDiscount.className = "info__discount";
            DOMInfoDiscount.innerText = `скидка - ${data.totalDiscount} ₽`;
            DOMInfoList.appendChild(DOMInfoDiscount);
        }
        DOMInfoBlock.appendChild(DOMInfoList);
        DOMBtn.className = "info__next";
        DOMBtn.onclick = (e) => setFullVariant(data, groupIndex);
        DOMInfoBlock.appendChild(DOMBtn);
    }
    DOMInfoList.className = "info__list";
    DOMInfoBlock.appendChild(DOMInfoList);
    DOMCont.appendChild(DOMInfoBlock);

    if (isFull) {
        DOMInfoBlock.appendChild(DOMMoreInfoBlock);
        DOMCont.appendChild(DOMBtnWrapper);
    }
    return DOMCont;
}

const buildHeaderDOM = (text, onclick) => {
    let DOMHeader = document.createElement("header"),
        DOMTitle = document.createElement("h1"),
        DOMBackBtn = document.createElement("button");

    DOMBackBtn.className = "back-btn";
    DOMBackBtn.onclick = onclick;
    DOMTitle.className = "variants__title";
    DOMTitle.innerHTML = text;
    DOMHeader.appendChild(DOMBackBtn);
    DOMHeader.appendChild(DOMTitle);
    return DOMHeader;
}

const setVariants = (title, data) => {
    document.querySelector(".page_active").classList.remove("page_active");
    let container = document.querySelector(".variants"),
        DOMWrapper = document.createElement("div");
    DOMWrapper.className = "page__wrapper";

    container.innerText = "";
    container.appendChild(buildHeaderDOM(`Тариф "${title}"`, setTariffs));

    data.sort((a, b) => (a.ID > b.ID) ? 1 : -1).map((element, i) => {
        DOMWrapper.appendChild(buildVariantDOM(element, false, i));
    });
    container.appendChild(DOMWrapper);
    container.classList.add("page_active");
}

const setFullVariant = (selectedTariffData, groupIndex) => {
    document.querySelector(".page_active").classList.remove("page_active");
    let container = document.querySelector(".variant-full"),
        DOMWrapper = document.createElement("div");
    DOMWrapper.className = "page__wrapper";
    container.innerText = "";

    container.appendChild(buildHeaderDOM("Выбор тарифа", (e) => setVariants(data[groupIndex].title, data[groupIndex].tarifs)));
    DOMWrapper.appendChild(buildVariantDOM(selectedTariffData, true));
    container.appendChild(DOMWrapper);
    container.classList.add("page_active");
}

const buildTariffDOM = (data, cont) => {

    let DOMCont = document.createElement("div"),
        DOMTitle = document.createElement("h3"),
        DOMInfoBlock = document.createElement("div"),
        DOMInfoSpeed = document.createElement("span"),
        DOMInfoPrice = document.createElement("span"),
        DOMLink = document.createElement("a"),
        DOMNextBtn = document.createElement("button");

    // Building list of free options
    function buildFreeListDOM() {
        if (!data.free_options) return;
        let DOMInfoFree = document.createElement("ul");

        DOMInfoFree.className = "info__list";

        data.free_options.map(elem => {
            let DOMli = document.createElement("li");
            DOMli.innerText = elem;
            DOMInfoFree.appendChild(DOMli);
        })

        return DOMInfoFree;
    }

    // Counting monthly prices
    let minPriceMonthly = Infinity,
        maxPriceMonthly = -Infinity;
    data.tarifs.forEach(elem => {
        elem.priceMonthly = elem.price / elem.pay_period;
        minPriceMonthly = Math.min(elem.priceMonthly, minPriceMonthly);
        maxPriceMonthly = Math.max(elem.priceMonthly, maxPriceMonthly);
    })
    // We have to do forEach again because in case of unsorted data we won't get maxPrice in the first iteration. 
    data.tarifs.forEach(elem => {
        elem.totalDiscount = (maxPriceMonthly - elem.priceMonthly) * elem.pay_period;
    })

    DOMCont.className = "tariffs__elem tariff";

    DOMTitle.className = "tariff__title";
    DOMTitle.innerText = `Тариф "${data.title}"`;
    DOMCont.appendChild(DOMTitle);

    DOMInfoBlock.className = "tariff__info info";
    let classColorMod = "";
    if (data.title.indexOf("Земля") >= 0)
        classColorMod = "color_earth";
    else if (data.title.indexOf("Огонь") >= 0)
        classColorMod = "color_fire";
    else if (data.title.indexOf("Вода") >= 0)
        classColorMod = "color_water";
    else
        classColorMod = "color_air";

    DOMInfoSpeed.className = `info__speed ${classColorMod}`;
    DOMInfoSpeed.innerText = `${data.speed} Мбит/с`;
    DOMInfoBlock.appendChild(DOMInfoSpeed);
    DOMInfoPrice.className = "info__price";
    DOMInfoPrice.innerText = `${minPriceMonthly} - ${maxPriceMonthly} ₽/мес`;
    DOMInfoBlock.appendChild(DOMInfoPrice);
    if (data.free_options) DOMInfoBlock.appendChild(buildFreeListDOM());

    DOMNextBtn.className = "info__next";
    DOMNextBtn.onclick = () => setVariants(data.title, data.tarifs);
    DOMInfoBlock.appendChild(DOMNextBtn);

    DOMCont.appendChild(DOMInfoBlock);

    DOMLink.className = "tariff__link";
    DOMLink.innerText = "узнать подробнее на сайте www.sknt.ru";
    DOMLink.href = data.link;
    DOMLink.target = "_blank";
    DOMCont.appendChild(DOMLink);


    return DOMCont;
}

const setTariffs = () => {
    document.querySelector(".page_active").classList.remove("page_active");
    let container = document.querySelector(".tariffs"),
        DOMWrapper = document.createElement("div");
    DOMWrapper.className = "page__wrapper";
    container.innerHTML = "";

    data.map(element => {
        DOMWrapper.appendChild(buildTariffDOM(element));
    });
    container.appendChild(DOMWrapper);
    container.classList.add("page_active");
}
var data = [];
window.onload = () => {
    getTariffs().then(res => {
        data = res;
        setTariffs();
    });
}