<?
    $json = file_get_contents('./data.json');
    $data = json_decode($json);

    //formatting dates
    foreach($data->tarifs as $tariffGroup) {
        foreach($tariffGroup->tarifs as $tariffProp) {
            $tariffProp->new_payday = date('d.m.Y', $tariffProp->new_payday);
        }
    }

    echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
?>